"use strict";


/*1. Опишіть своїми словами, що таке метод об'єкту.

	Метод объекта - это функция, которая присвоена одному из свойств объекта в качестве его значения. 
	Такие функции выполняют работу, используя свойства текущего объекта в качестве данных. 
	Что бы достучатся до свойств текущего обекта нужно использовать ключевое слово this. 
	Это позволяет сохранить работоспособность методов в независимости от того, по какой ссылке обращаются к данному объекту.
*/

/* 2. Який тип даних може мати значення властивості об'єкта?

	Свойство объекта - это строка, а значение этого свойства может иметь любой тип данных который существует в js. 
*/

/* 3. Об'єкт це посилальний тип даних. Що означає це поняття? 

	Ссылочный тип данных - это означает, что когда мы создаем объект и присваеваем его какой-то переменной, то в этой переменной будет хранится лишь ссылка на объект, а не он сам.
	Поэтому при переприсваивании переменной в другую переменную, мы не создаем копию объекта а лишь копируем ссылку на этот объект. 
	После этого можно будет получать доступ к одному объекту из нескольких разных мест (переменных).  
*/



// function createNewUser(firstName = 'Empty', lastName = 'Empty') {

// 	const newUser = {

// 		_firstName: firstName,
// 		_lastName: lastName,

// 		get firstName() {
// 			return this._firstName;
// 		},

// 		get lastName() {
// 			return this._lastName;
// 		},

// 		set firstName(value) {
// 			if (/\d/g.test(value)) {
// 				alert("Ви ввели не корректне ім'я!");
// 				return this.firstName = prompt("Повторіть, будь ласка ввід");
// 			};
// 			this._firstName = value.trim() || firstName;
// 		},

// 		set lastName(value) {
// 			while (/\d/g.test(value)) {
// 				alert("Ви ввели не корректне прізвище!");
// 				return this.lastName = prompt("Повторіть, будь ласка ввід");
// 			};
// 			this._lastName = value.trim() || lastName;
// 		},

// 		getLogin() {
// 			if (this.firstName !== 'Empty' && this.lastName !== 'Empty') {
// 				return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
// 			} else {
// 				throw new Error("Щоб отримати логін введіть корректно всі данні!");
// 			}
// 		}
// 	}

// 	newUser.firstName = prompt("Введіть ваше ім'я", "Ivan") || firstName;
// 	newUser.lastName = prompt("Введіть ваше прізвище", "Petrenko") || lastName;

// 	Object.defineProperties(newUser, {
// 		'_firstName': {
// 			enumerable: false,
// 			configurable: false,
// 		},
// 		'_lastName': {
// 			enumerable: false,
// 			configurable: false,
// 		},
// 		'getLogin': {
// 			configurable: false,
// 		},
// 	});

// 	return newUser;
// };

// const newUser = createNewUser();
// console.log(newUser);

// console.log(Object.entries(newUser));
// console.log(`Логін: ${newUser.getLogin()}`);

// newUser.firstName = " Petro";
// newUser.lastName = " Pupkin ";

// console.log(Object.entries(newUser));
// console.log(`Логін: ${newUser.getLogin()}`);

// newUser.firstName = " ";
// newUser.lastName = " Ivanov ";

// console.log(Object.entries(newUser));
// console.log(`Логін: ${newUser.getLogin()}`);


function createAnotherUser(firstName = 'Empty', lastName = 'Empty') {

	const newUser = {

		firstName: prompt("Введіть ваше ім'я", "Ivan") || firstName,
		lastName: prompt("Введіть ваше прізвище", "Petrenko") || lastName,

		setFirstName(newFName) {
			if (/\d/g.test(newFName)) {
				throw new Error("Ви ввели не корректне ім'я!");
			} else {
				Object.defineProperty(this, 'firstName', {
					value: newFName.trim() || firstName,
				});
			}
		},

		setLastName(newFName) {
			if (/\d/g.test(newFName)) {
				throw new Error("Ви ввели не корректне прізвище!");
			} else {
				Object.defineProperty(this, 'lastName', {
					value: newFName.trim() || lastName,
				});
			}
		},

		getLogin() {
			if (this.firstName !== 'Empty' && this.lastName !== 'Empty') {
				return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
			} else {
				throw new Error("Щоб отримати логін введіть корректно всі данні!");
			}
		}
	}

	Object.defineProperties(newUser, {
		'firstName': {
			writable: false,
			configurable: true,
		},
		'lastName': {
			writable: false,
			configurable: true,
		},
		'getLogin': {
			writable: false,
			configurable: false,
		},
		'setLastName': {
			writable: false,
			enumerable: false
		},
		'setFirstName': {
			writable: false,
			enumerable: false
		},
	});
	return newUser;
};

const newUser = createAnotherUser();
console.log(newUser);
console.log(Object.entries(newUser));
console.log(`Логін: ${newUser.getLogin()}`);

// console.log('__TEST_1__');
// newUser.firstName = "Martin";
// newUser.lastName = "Perez";
// console.log(newUser);

console.log('__TEST_2__');
// newUser.setFirstName("123_test");
newUser.setLastName("Perez");
console.log(newUser);

// console.log('__TEST_3__');
// newUser.setFirstName(" ");
// console.log(newUser);
// console.log(Object.entries(newUser));
// console.log(`Логін: ${newUser.getLogin()}`);